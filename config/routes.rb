Rails.application.routes.draw do
  devise_for :users

  get 'all_users', to: 'users#show_all_users'
  get 'friends', to: 'users#show_friends'
  get 'main', to: 'index#index', as: :index
  resources :users do
    resources :profiles
    member do
      get :follow
      get :unfollow
    end
  end
  resources :walls
  root 'users#index'
end
