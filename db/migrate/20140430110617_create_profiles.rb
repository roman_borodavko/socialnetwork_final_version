class CreateProfiles < ActiveRecord::Migration
  def change
    remove_columns :users,:last_name,:name,:middle_name,:phone_number,:skype,:photo,:image,:address
    create_table :profiles do |t|
      t.string :last_name
      t.string :name
      t.string :middle_name
      t.integer :phone
      t.string :skype
      t.string :image
      t.date :dob
      t.string :address

      t.timestamps
    end
  end
end
