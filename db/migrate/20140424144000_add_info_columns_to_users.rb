class AddInfoColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_name,    :string
    add_column :users, :name,         :string
    add_column :users, :middle_name,  :string
    add_column :users, :image,        :string
    add_column :users, :address,      :string
    add_column :users, :phone_number, :integer
    add_column :users, :photo,        :string
  end
end
