class ProfilesController < ApplicationController
  before_filter :find_profile, only:[:show,:edit,:destroy,:update]
  before_action :set_user

  def index
    @profiles = @user.profile
    unless @profile
      render 404
    end
  end

  def new
    @profile = Profile.new
  end

  def create
    @profile = @user.build_profile(profile_params)
    if @profile.save
      redirect_to root_path
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @profile.update_attributes(profile_params)
      redirect_to root_path
    else
      render :edit
    end
  end

  def destroy
    @profile.destroy
  end

  private

  def find_profile
    @profile = Profile.find(params[:id])
    unless @profile
      render_404
    end
  end

  def profile_params
    params.require(:profile).permit(:last_name,:name,:middle_name,:phone,:address,:skype,:dob,:image)
  end

  def set_user
    @user = User.find(params[:user_id])
  end
end