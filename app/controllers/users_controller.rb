class UsersController < ApplicationController

  def index
    @user = current_user
    @friends_1 = @user.all_following
    @friends_2 = @user.followers
    @friends = (@friends_1 + @friends_2).uniq
    @profile = @user.profile
    @wall = @user.walls.where(user_id: @user.id).order('created_at desc')
  end

  def show
    @user = User.find(params[:id])
    @wall = @user.walls.where(user_id: params[:id]).order('created_at desc')
    @friends_1 = @user.all_following
    @friends_2 = @user.followers
    @friends = (@friends_1 + @friends_2).uniq
    if @user
      @profile = @user.profile
    else
      render_404
    end
  end

  def show_friends
    @user = current_user
    @friends_1 = @user.all_following
    @friends_2 = @user.followers
    @friends = (@friends_1 + @friends_2).uniq
    @friends_count = @friends.count
  end

  def show_all_users
    @user = current_user
    @users_count = User.count
    @users = User.all.page params[:page]
  end

  def follow
    @user = User.find(params[:id])
    current_user.follow(@user)
  end

  def unfollow
    @user = User.find(params[:id])
    if current_user.following?(@user)
      current_user.stop_following(@user)
    elsif @user.following?(current_user)
      @user.stop_following(current_user)
    end
  end

#-------------------------------------------
  def create

  end

  def edit

  end

  def new

  end

  def update

  end

  def destroy

  end

end