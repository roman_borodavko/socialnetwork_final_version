class WallsController < ApplicationController
  def create
    @wall = Wall.new(wall_params)
    @wall.save
  end

  def destroy
    @wall = Wall.find(params[:id])
    @wall.destroy
  end

  private
  def wall_params
    params.require(:wall).permit(:user_id,:writer_id,:text)
  end
end