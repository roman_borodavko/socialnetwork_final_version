class FriendsController < ApplicationController
  def create
    @friend = Friend.new(params[:friend])
    @friend.save
  end

  def destroy

  end

  private
  def friend_params
    params.require(:friend).permit(:user_id,:friend_id)
  end

end