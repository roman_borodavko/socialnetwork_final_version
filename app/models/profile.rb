class Profile < ActiveRecord::Base
  belongs_to :user
  validates :last_name,:name,:dob, presence: true
  mount_uploader :image, ImageUploader
end
