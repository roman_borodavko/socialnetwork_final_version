class User < ActiveRecord::Base
  rolify
  has_one  :profile
  has_many :walls
  has_many :friends

  paginates_per 3

  acts_as_follower
  acts_as_followable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
