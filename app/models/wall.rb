class Wall < ActiveRecord::Base
  belongs_to :user
  validates :user_id, :writer_id, presence: true
  resourcify
end
